package com.example.movieapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.movieapp.entity.UserMovie;

@Repository
public interface UserMovieRepository extends CrudRepository<UserMovie, Long> {
	
	

}
