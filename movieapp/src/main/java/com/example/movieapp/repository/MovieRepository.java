package com.example.movieapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.movieapp.entity.Movie;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long> {

}
