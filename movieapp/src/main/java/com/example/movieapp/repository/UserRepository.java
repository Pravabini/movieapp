package com.example.movieapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.movieapp.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
