package com.example.movieapp.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="user_movie")
public class UserMovie {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long userMovieId;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name="movie_id")
	private Movie movie;
	
	private LocalDate watchedDate;
	
	
}
