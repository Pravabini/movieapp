package com.example.movieapp.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="movie")
public class Movie {
	
	@Id
	private long movieId;
	
	private String movieName;
	
	private String movieType;
	
	private String movieDescription;
	
	private LocalDate movieReleaseDate;
	
	private String language;
	
	
	
	
	
	
	

}
