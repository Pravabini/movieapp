package com.example.movieapp.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="user_account")
public class User {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long userId;
	
	private String userName;
	
	private double phoneNumber;
	
	@Column(unique=true)
	private String email;
	
	private String password;
	
	private String userType;
	
	private LocalDateTime registrationDate;
	
	private LocalDateTime modifiedDate;
	
	private String userAccountStatus;
	
	
}
